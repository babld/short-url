<?php

namespace app\controllers;

use app\components\Shorten;
use app\models\UrlForm;
use app\models\Urls;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex($code = false)
    {
        if($code !== false) {
            if ($model = Shorten::shortCodeToUrl($code)) {
                return $this->redirect($model->url);
            }

            return $this->goHome();
        }

        if (yii::$app->request->isPost && $post = yii::$app->request->post()) {
            $model = new UrlForm();
            if($model->load($post)) {
                Shorten::urlToShortCode($model->url);
            }
        }

        return $this->render('index', [
            'models' => Urls::find()->limit(10)->orderBy('id DESC')->all(),
            'model' => new UrlForm()
        ]);
    }

    /*public function actionRedirect($code) {
        if ($model = Shorten::shortCodeToUrl($code)) {
            return $this->redirect($model->url);
        }

        return $this->goHome();
    }*/
}

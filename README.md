<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Test project based on Yii 2 Basic Template</h1>
    <br>
</p>

INSTALLATION
------------
### 1) Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following command:

~~~
composer install
~~~

### 2) Fix web server DocumentRoot 
DocumentRoot folder to [path-to-project]/web

### 3) Migration

~~~
php yii migrate
~~~

### 4) Check access rights to folder runtime and assets
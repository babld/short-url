<?php

namespace app\models;

use yii\base\Model;

class UrlForm extends Model
{
    public $url;

    public function rules()
    {
        return [
            ['url', 'required'],
            ['url', 'url']
        ];
    }

    public function attributeLabels()
    {
        return [
            'url' => 'Insert URL',
        ];
    }
}
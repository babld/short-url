<?php

namespace app\models;

class Urls extends \yii\db\ActiveRecord
{
    function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::class,
        ];
    }

    public static function tableName()
    {
        return 'urls';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url', 'short_code', 'hits'], 'required'],
            [['url', 'short_code'], 'string', 'max' => 500],
            ['url', 'url'],
            [['hits', 'created_at', 'updated_at'], 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'short_code' => 'Short code',
            'hits' => 'Hits count',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At'
        ];
    }
}

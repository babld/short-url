<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$yiiRequest = \yii::$app->request;
$scheme = "http" . ((!empty($_SERVER['HTTPS']) and $_SERVER['HTTPS'] == 'on') ? 's' : '');
$hostName = $yiiRequest->hostName;
?>

<?php $form = ActiveForm::begin() ?>

<?= $form->field($model, 'url')->textInput(['placeholder' => 'Вставьте ссылку'])->label('Вставьте ссылку') ?>

<?= Html::tag('div', Html::submitButton('Сократить ссылку'), ['class' => 'form-group']) ?>

<?php ActiveForm::end() ?>

<?php if (!empty($models)): ?>
    <?php foreach ($models as $model): ?>
        <?php
        $content = $scheme . '://' . $hostName . "/$model->short_code";
        ?>
        <?= Html::tag(
            'div',
            Html::a($content, $content, ['target' => '_blank']) . Html::tag('span', $model->hits),
            ['class' => 'short-code']
        ) ?>
        <?= Html::tag('div', $model->url, ['class' => 'original-link']) ?>
        <hr/>
    <?php endforeach ?>
<?php endif ?>






<?php

namespace app\components;

use Yii;
use app\models\Urls;

class Shorten
{
    protected static $chars = "abcdfghjkmnpqrstvwxyz|ABCDFGHJKLMNPQRSTVWXYZ|0123456789";
    protected static $checkUrlExists = false;
    protected static $codeLength = 6;

    public static function urlToShortCode($url)
    {
        if (empty($url)) {
            Yii::$app->session->setFlash('error', 'Пустой URL.');
            return false;
        }

        if (self::validateUrlFormat($url) == false) {
            Yii::$app->session->setFlash('error', 'Не верный формат URL.');
            return false;
        }

        if (self::$checkUrlExists) {
            if (!self::verifyUrlExists($url)) {
                Yii::$app->session->setFlash('error', 'Не существующий URL');
                return false;
            }
        }

        $shortCode = self::urlExistsInDB($url);

        if ($shortCode == false) {
            $shortCode = self::createShortCode($url);
        }

        return $shortCode;
    }

    protected function validateUrlFormat($url)
    {
        return filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED);
    }

    protected function verifyUrlExists($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return (!empty($response) && $response != 404);
    }

    protected function createShortCode($url)
    {
        # Если сгенерированный код уже есть в базе, генерим новый
        do {
            $shortCode = self::generateRandomString(self::$codeLength);
        } while (self::getUrlFromDB($shortCode));

        if (self::insertUrlInDB($url, $shortCode)) {
            return $shortCode;
        }

        return false;
    }

    protected function generateRandomString($length = 6)
    {
        $sets = explode('|', self::$chars);
        $all = '';
        $randString = '';
        foreach ($sets as $set) {
            $randString .= $set[array_rand(str_split($set))];
            $all .= $set;
        }

        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++) {
            $randString .= $all[array_rand($all)];
        }
        $randString = str_shuffle($randString);
        return $randString;
    }

    public static function shortCodeToUrl($code, $increment = true)
    {
        if (self::validateShortCode($code) == false) {
            Yii::$app->session->setFlash('error', 'Данный короткий код не валиден');
            return false;
        }

        $model = self::getUrlFromDB($code);

        if (!$model) {
            Yii::$app->session->setFlash('error', 'Данный короткий код не существует');
            return false;
        }

        if ($increment == true) {
            $model->hits += 1;
            $model->update(false);
        }

        return $model;
    }

    public function getUrlFromDB($code)
    {
        if ($model = Urls::findOne(['short_code' => $code])) {
            return $model;
        }
        return false;
    }

    protected function validateShortCode($code)
    {
        $rawChars = str_replace('|', '', self::$chars);
        return preg_match("|[" . $rawChars . "]+|", $code);
    }

    protected function insertUrlInDB($url, $code)
    {
        $model = new Urls();
        $model->url = $url;
        $model->short_code = $code;
        $model->hits = 0;

        if ($model->save()) {
            return true;
        }

        return false;
    }

    protected function urlExistsInDB($url)
    {
        if ($model = Urls::findOne(['url' => $url])) {
            return $model->short_code;
        }

        return false;
    }
}
